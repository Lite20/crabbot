const fs = require('fs');
const Eris = require("eris");
const nano = require('nano')('http://localhost:5984');

const Config = require("./config.json");

const Quote = require("./cmd/quote");
const Suggest = require("./cmd/suggest");
const Poll = require("./cmd/poll");
const Remind = require("./cmd/remind");
const Help = require("./cmd/help");

var quotes;

nano.db.create('crab-quotes', (err, data) => {
    quotes = nano.db.use('crab-quotes');
});

var bot = new Eris(Config.TOKEN, {
    largeThreshold: 1 // how many offline users to grab
});

bot.on("ready", () => {
    console.log("Connected to Discord");
    bot.editStatus("online", {
        name: "Aseprite",
        type: 0
    });
});

bot.on("messageCreate", (msg) => {
    let cmd;
    if (msg.content.startsWith("!")) cmd = msg.content.split(" ")[0].split("!")[1];
    else return;

    if (cmd == "help" || cmd == "h") Help(bot, msg);
    else if (cmd == "suggest") Suggest(bot, msg, Config);
    else if (cmd == "poll") Poll(bot, msg, Config);
    else if (cmd == "quote" || cmd == "q") Quote(bot, msg, quotes);
    else if (cmd == "remind" || cmd == "r") Remind(bot, msg);
});

/*
bot.on("disconnect", (msg, id) => {
    console.log("connection lost.");
});

bot.on("debug", (msg, id) => {
    console.log(id + ": " + msg);
});

bot.on("error", (err, id) => {
    console.log(id + "!: " + err);
});

bot.on("warn", (msg, id) => {
    console.log(id + ": " + msg);
});
*/

bot.connect();
