const profile_util = require('./profile');

var xp_flags = {};
var updates = 0;
module.exports = {
    performUpdate: function(user_db, delay, xp_amount, token_amount) {
        setInterval(() => {
            for (var id in xp_flags) {
                if (xp_flags[id]) {
                    xp_flags[id] = false;
                    user_db.get(id, function(err, user) {
                        if (err) {
                            if (err.error !== "not_found") return console.log(err);
                            user = profile_util.init_user(user_db, id);
                        }

                        user.xp += xp_amount;
                        user.bits += token_amount;
                        user_db.insert(user, id, function(err, body) {
                            if (err) console.log(err);
                        });
                    });
                }
            }

            // clear flag map
            updates++;
            if(updates > 5) {
                updates = 0;
                xp_flags = {};
            }
        }, delay);
    },
    xp_flag: function(id) {
        xp_flags[id] = true;
    }
};
