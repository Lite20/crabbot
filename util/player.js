module.exports = function (bot, link) {
    bot.joinVoiceChannel(voice_queue[0].channel).catch((err) => {
        console.log(err);
    }).then((connection) => {
        let path = voice_queue[0].path;
        connection.play(voice_queue[0].path);
        voice_queue.shift();
        connection.once("end", () => {
            if (voice_queue.length > 0) setTimeout(handle_queue, 1);
            fs.unlink(path, (err) => {
                if (err) throw err;
            });
        });
    });
}
