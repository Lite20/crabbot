module.exports = function (bot, msg) {
    bot.createMessage(msg.channel.id, {
        embed: {
            title: "Crabbot Help",
            color: 0xffb320,
            fields: [
                {
                    name: "!suggest [...]",
                    value: "Make a suggestion! Just type it after !suggest",
                    inline: false
                },
                {
                    name: "!poll <title>: [option 1, option 2, ...]",
                    value: "Create a poll that people can vote on",
                    inline: false
                }
            ],
            footer: {
                text: "For questions or support, contact döpplo#0001"
            }
        }
    });
};
