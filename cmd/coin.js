module.exports = function (bot, msg) {
    if (Math.round(Math.random()) == 1)
        bot.createMessage(msg.channel.id,  "<@" + msg.author.id + "> You flipped a coin. It's heads!");
    else
        bot.createMessage(msg.channel.id,  "<@" + msg.author.id + "> You flipped a coin. It's tails!");
};
