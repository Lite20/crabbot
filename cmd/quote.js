module.exports = function (bot, msg, quotedb) {
    let parts = msg.content.split(" ");

    if (parts.length > 2) {
        // quote someone else
        if (msg.mentions.length > 0) {
            quotedb.insert({
                author: msg.mentions[0].username,
                quote: msg.content,
                time: Date.now() / 1000 | 0 // bitwise or to floor
            });
        }

        // quote self
        else {
            quotedb.insert({
                author: msg.author.username,
                quote: msg.content,
                time: Date.now() / 1000 | 0 // bitwise or to floor
            });
        }

        msg.addReaction(":ClawsUp:500832552789999617", "@me");
    }

    // fetch quote
    else {
        bot.createMessage(config.SUGGEST_CHANNEL, "beep, bop! <incomplete>");
    }
};
