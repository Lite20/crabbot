const Sugar = require('sugar');

/*
    TODO:
    - store timezone of users and get it from them to adjust the times
    - store the reminder
*/
module.exports = function (bot, msg) {
    // check if we have the right number of quotations
    if ((msg.content.split("\"").length - 1) != 2) {
        bot.createMessage(msg.channel.id, "<@" + msg.author.id + "> Be sure to only put quotations around what you want me to remind you of!");
        return;
    }

    // check if anyone was tagged
    if (msg.mentions.length < 1) {
        bot.createMessage(msg.channel.id, "<@" + msg.author.id + "> You didn't tag anyone. Who am I reminding?");
        return;
    }

    // provide support for times that sugar doesn't naturally support (they're subjective)
    msg.content = msg.content.replace("morning", "7am");
    msg.content = msg.content.replace("brunch", "10am");
    msg.content = msg.content.replace("lunch", "12pm");
    msg.content = msg.content.replace("lunch time", "12pm");
    msg.content = msg.content.replace("evening", "5pm");
    msg.content = msg.content.replace("dinner", "7pm");
    msg.content = msg.content.replace("night", "10pm");

    // extract the date
    const dateSegment = msg.content.substr(msg.content.lastIndexOf('"') + 1).trim();
    const date = Sugar.Date.create(dateSegment);

    if (date == "Invalid Date") {
        bot.createMessage(msg.channel.id, "<@" + msg.author.id + "> Boop, beep! I don't know when \"" + dateSegment + "\" is! Please try again.");
        return;
    }

    // extract the reminder
    const fIndex = msg.content.indexOf('"') + 1;
    const lIndex = msg.content.lastIndexOf('"');
    const remindSegment = msg.content.substr(fIndex, lIndex - fIndex);

    // get the target
    let target = msg.mentions[0];
    console.log(target);

    bot.createMessage(msg.channel.id, {
        embed: {
            title: "Reminder Set!",
            color: 0xffb320,
            fields: [
                {
                    name: date.toLocaleString(),
                    value: "Remind " + target.username + " \"" + remindSegment + "\"",
                    inline: false
                }
            ],
            footer: {
                text: "For questions or support, contact döpplo#0001"
            }
        }
    });
};
