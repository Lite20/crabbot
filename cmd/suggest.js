module.exports = function (bot, msg, config) {
    let suggestion = msg.content.slice(9);
    bot.createMessage(
        config.SUGGEST_CHANNEL,
        "**" + msg.author.username + "'s suggestion:** " + suggestion
    ).then((message) => {
        message.addReaction("⬆", "@me");
        message.addReaction("⬇", "@me");
    });

    bot.createMessage(
        msg.channel.id,
        "Ah okay. You can vote on it in <#" + config.SUGGEST_CHANNEL + ">"
    );
};
