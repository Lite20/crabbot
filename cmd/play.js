const YoutubeMp3Downloader = require("youtube-mp3-downloader");
const url = require('url');

const YD = new YoutubeMp3Downloader({
    "ffmpegPath": config.ffmpeg,        // Where is the FFmpeg binary located?
    "outputPath": "/vid/",    // Where should the downloaded and encoded files be stored?
    "youtubeVideoQuality": "highest",       // What video quality should be used?
    "queueParallelism": 20,                  // How many parallel downloads/encodes should be started?
    "progressTimeout": 20000                 // How long should be the interval of the progress reports
});

var queue = [];
//Configure YoutubeMp3Downloader with your settings

module.exports = function (bot, msg, config) {
    let link = msg.content.slice(6);
    let url = new URL("https://www.youtube.com/watch?v=UuaWxCwnZE4");

    //Download video and save as MP3 file
    YD.download("Vhd6Kc4TZls");

    YD.on("finished", function(err, data) {
        console.log(JSON.stringify(data));
    });

    YD.on("error", function(error) {
        console.log(error);
    });

    YD.on("progress", function(progress) {
        console.log(JSON.stringify(progress));
    });
    
    player.addToQueue();
    bot.createMessage(
        msg.channel.id,
        "added to queue. "
    );
};
