const tags = [
    "🇦", "🇧", "🇨", "🇩", "🇪", "🇫", "🇬", "🇭", "🇮", "🇯", "🇰", "🇱", "🇲", "🇳", "🇴", "🇵",
    "🇶", "🇷", "🇸", "🇹", "🇺", "🇻", "🇼", "🇽", "🇾", "🇿"
];

module.exports = function (bot, msg, config) {
    let line = msg.content.slice(6);
    if (line.length <= 0) return;
    if (typeof line.split(":")[1] == "undefined") return;
    if (line.split(":")[1].length <= 0) return;

    let title = line.split(":")[0];
    if (title.length <= 0) return;

    let options = line.split(":")[1].split(",");
    if (options.length <= 0) return;

    let entries = [];
    for (var i = 0; i < options.length; i++) {
        entries.push({
            name: tags[i],
            value: options[i].trim(),
            inline: false
        });
    }

    let d = new Date();
    bot.createMessage(
        config.POLL_CHANNEL,
        {
            embed: {
                title: "[Question] " + title,
                author: {
                    name: msg.author.username + "'s Poll",
                    icon_url: msg.author.avatarURL
                },
                color: 0xffb320,
                fields: entries,
                footer: {
                    text: "Please react to vote. Created on " + d.getDate() + "/" + d.getMonth() + "/" + d.getFullYear()
                }
            }
        }
    ).then((message) => {
        for (var i = 0; i < options.length; i++)
            message.addReaction(tags[i], "@me");
    });

    bot.createMessage(
        msg.channel.id,
        "Poll created in <#" + config.POLL_CHANNEL + ">"
    );
};
